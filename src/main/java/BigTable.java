import com.google.cloud.bigtable.hbase.BigtableConfiguration;
import com.sun.istack.internal.NotNull;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

;

public class BigTable {


    private static final String PROJECT_ID = "sabd1718-pt";
    private static final String instanceId = "trottapelella";

    /**
     * Questo metodo serve per settare la variabile d'ambiente, la quale è richiesta per eseguire le api di GCP
     *
     * @param key   nome della variabile d'ambiente che si vuole creare
     * @param value valore della variabile d'ambiente
     */
    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }


    /*public static void main(String[] args) {

// Create the Bigtable connection, use try-with-resources to make sure it gets closed
    try {

        BigTable.setEnv("GOOGLE_APPLICATION_CREDENTIALS","/Users/Valerio/Downloads/SABD1718-PT-a36dfe7ac719.json");

        Connection connection = BigtableConfiguration.connect(PROJECT_ID, instanceId);


        ArrayList<String> column_family=new ArrayList<>();
        column_family.add("DifferenzaFascia");
        ArrayList<String> column_name=new ArrayList<>();
        column_name.add("DifferenzaFascia");

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

        deleteAllRow(connection,"terzaQuery",column_family,column_name);

       *//* try {
            createTable(admin, "nuova_tabella", "famiglia1","famiglia2");
        }
        catch (TableExistsException e){
            System.out.println("tabella già esistente");
        }*//*

     *//*try{
            deleteTable(admin,connection,"primaQuery");
            System.out.println("cancellata la tabella che esisteva precedentemente");
        }
        catch (TableNotFoundException e){
        }*//*



        //readRow(connection,"nuova_tabella","gianni","famiglia1","età");
        //readAllRow(connection,"nuova_tabella","famiglia1","età");

        //readAllTables(connection,"nuova_tabella",column_family,column_name);
        //deleteTable(admin,connection,"nuova_tabella");
    }

        catch(IOException e){
            System.err.println("erroreeee\n");
            e.printStackTrace();
            System.exit(1);
        }

    }*/

    /**
     * Questo metodo serve per creare una connessione con BigTable sfruttando le variabili globali PROJECT_ID e instanceID
     */

    public static void createConnection() throws IOException {
        // Create the Bigtable connection, use try-with-resources to make sure it gets closed
        Connection connection = BigtableConfiguration.connect(PROJECT_ID, instanceId);

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

    }

    /**
     * Questo metodo serve per creare una tabella con una serie di column family
     *
     * @param admin              admin di gcp
     * @param TABLE_NAME         nome della tabella che si intende creare
     * @param COLUMN_FAMILY_NAME nome della famiglia di colonne che si vuole creare
     */
    public static void createTable(Admin admin, String TABLE_NAME, String... COLUMN_FAMILY_NAME) throws IOException {
        // Create a table with a single column family
        HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(TABLE_NAME));
        for (String s : COLUMN_FAMILY_NAME)
            descriptor.addFamily(new HColumnDescriptor(s));
        admin.createTable(descriptor);
        System.out.println("Create table " + descriptor.getNameAsString());

    }

    /**
     * Questo metodo serve per creare una nuova riga all'interno di una tabella
     *
     * @param connection         connessione con gcp
     * @param TABLE_NAME         nome della tabella in cui inserire la riga
     * @param COLUMN_FAMILY_NAME nome della famiglia di colonne in cui si vuole inserire il nuovo elemento
     * @param COLUMN_NAME        nome della colonna in cui si vuole inserire il nuovo elemento
     * @param rowKey             chiave da assegnare alla nuova riga
     * @param valueToInsert      valore da inserire nella riga
     */

    public static void createRow(Connection connection, String TABLE_NAME, String COLUMN_FAMILY_NAME, String COLUMN_NAME, @NotNull String rowKey, @NotNull String valueToInsert) throws IOException {
        // Retrieve the table we just created so we can do some reads and writes
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

        // Put a single row into the table. We could also pass a list of Puts to write a batch.
        Put put = new Put(Bytes.toBytes(rowKey));
        put.addColumn(Bytes.toBytes(COLUMN_FAMILY_NAME), Bytes.toBytes(COLUMN_NAME), Bytes.toBytes(valueToInsert));
        table.put(put);
        // Write some rows to the table
        System.out.println("Write some greetings to the table");
    }

    /**
     * Questo metodo serve per leggere una riga da BigTable. Bisogna avere la chiave della riga da leggere
     *
     * @param connection         connessione con bigtable
     * @param TABLE_NAME         nome della tabella da cui leggere il valore
     * @param rowKey             chiave della riga da  leggere
     * @param COLUMN_FAMILY_NAME nome della famiglia di colonne in cui c'è l'elemento che si vuole leggere
     * @param COLUMN_NAME        nome delle colonna in cui c'è l'ememnto che si vuole leggere
     */

    public static void readRow(Connection connection, String TABLE_NAME, String rowKey, String COLUMN_FAMILY_NAME, String COLUMN_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

        // Get the first greeting by row key
        Result getResult = table.get(new Get(Bytes.toBytes(rowKey)));
        String greeting = Bytes.toString(getResult.getValue(Bytes.toBytes(COLUMN_FAMILY_NAME), Bytes.toBytes(COLUMN_NAME)));
        System.out.println("Get a single greeting by row key");
        System.out.printf("\t%s = %s\n", rowKey, greeting);
    }


    /**
     * Questo metodo serve per leggere tutte le righe da una tabella di bigtable
     *
     * @param connection         connessione con bigtable
     * @param TABLE_NAME         nome della tabella da cui leggere il valore
     * @param COLUMN_FAMILY_NAME nome della famiglia di colonne in cui c'è l'elemento che si vuole leggere
     * @param COLUMN_NAME        nome delle colonna in cui c'è l'ememnto che si vuole leggere
     */

    public static void readAllRow(Connection connection, String TABLE_NAME, String COLUMN_FAMILY_NAME, String COLUMN_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        System.out.println("Scan for all greetings:");
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            byte[] valueBytes = row.getValue(Bytes.toBytes(COLUMN_FAMILY_NAME), Bytes.toBytes(COLUMN_NAME));
            System.out.println('\t' + Bytes.toString(valueBytes));
        }
    }

    /**
     * Questo metodo serve per leggere tutte le righe da una tabella di bigtable
     *
     * @param connection         connessione con bigtable
     * @param TABLE_NAME         nome della tabella da cui leggere il valore
     * @param COLUMN_FAMILY_NAME nome della famiglia di colonne in cui c'è l'elemento che si vuole leggere
     * @param COLUMN_NAME        nome delle colonna in cui c'è l'ememnto che si vuole leggere
     */

    public static void readAllInTable(Connection connection, String TABLE_NAME, ArrayList<String> COLUMN_FAMILY_NAME, ArrayList<String> COLUMN_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        System.out.println("Scan for all greetings:");
        System.out.println("tabella: " + TABLE_NAME);
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            for (String s : COLUMN_FAMILY_NAME) {
                for (String s1 : COLUMN_NAME) {
                    byte[] valueBytes = row.getValue(Bytes.toBytes(s), Bytes.toBytes(s1));

                    if (valueBytes != null)
                        System.out.println("key:" + Bytes.toString(row.getRow()) + " value:" + s + ":" + s1 + " " + Bytes.toString(valueBytes));
                }
            }

        }


    }


    /**
     * Questo metodo serve per cancellare una tabella da bigtable
     *
     * @param admin      admin di gcp
     * @param connection connessione con bigtable
     * @param TABLE_NAME nome della tabella che si vuole cancellare
     */

    public static void deleteTable(Admin admin, Connection connection, String TABLE_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

        admin.disableTable(table.getName());
        admin.deleteTable(table.getName());
        System.out.println("Cancello la tabella precedente di nome " + TABLE_NAME);

    }


    public static void deleteAllRow(Connection connection, String TABLE_NAME, ArrayList<String> COLUMN_FAMILY_NAME, ArrayList<String> COLUMN_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        System.out.println("Scan for all greetings:");
        System.out.println("tabella: " + TABLE_NAME);
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            System.out.println(Bytes.toString(row.getRow()));
            System.out.println(Bytes.toString(row.getValue(Bytes.toBytes("DifferenzaFascia"), Bytes.toBytes("DifferenzaFascia"))));

            Delete delete = new Delete(row.getRow());
            delete.addColumns(Bytes.toBytes("DifferenzaFascia"), Bytes.toBytes("DifferenzaFascia")); // the 's' matters
            table.delete(delete);
        }


    }


}
