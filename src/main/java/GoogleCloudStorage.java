import Utils.ReadFromArgs;
import com.google.cloud.storage.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;

public class GoogleCloudStorage {


    /*
    Questa è la parte iniziale del programma. Serve per caricare i file all'interno del google cloud storage.
    Negli argomenti bisogna specificare il path del file contenente le credenziali di google, il path del file da inserire,
    il bucket name che si vuole creare e il nome del file che si vuole inserire all'interno del bucket.
    */


    public static void main(String[] args) {

        //prendo i path dei file su google cloud storage
        Map<String, String> pathFiles = ReadFromArgs.readArgs(args);
        String pathGoogleCredential = new String();
        String pathInputDataset = new String();
        String bucketName = new String();
        String fileName = new String();


        try {
            pathGoogleCredential = pathFiles.get("pathGoogleCred");
            pathInputDataset = pathFiles.get("pathFileInput");
            bucketName = pathFiles.get("bucketName");
            fileName = pathFiles.get("fileName");
            ReadFromArgs.checkPathStrings(pathGoogleCredential, pathInputDataset, bucketName, fileName);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Usage: --pathGoogleCred ... --pathFileInput ... --bucketName ... --fileName ...");
            System.exit(1);
        }

        //setto la variabile d'ambiente Google_Application_Credentials con il path del file contenente le chiavi pubbliche e private del cloud di google.
        Publisher.setEnv("GOOGLE_APPLICATION_CREDENTIALS", pathGoogleCredential);


        try {
            writeToGCS(pathInputDataset, bucketName, fileName);
        } catch (StorageException e) {
            System.err.println("provare a cambiare nome bucket o nome file\n\n\n");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }


    /**
     * API creata per scrivere su google cloud storage
     *
     * @param pathInput  path del file che si vuole inserire
     * @param bucketName nome del bucket che si vuole creare
     * @param blobName   nome del file che si vuole creare all'interno del gcs
     */

    public static void writeToGCS(String pathInput, String bucketName, String blobName) throws IOException {
        // Create your service object
        Storage storage = StorageOptions.getDefaultInstance().getService();

// Create a bucket
        //String bucketName = "provabucket3"; // Change this to something unique
        Bucket bucket = storage.create(BucketInfo.of(bucketName));

// Upload a blob to the newly created bucket
        //String path="/Users/Valerio/Downloads/primaQuery/part-00000";
        byte[] data1 = Files.readAllBytes(new File(pathInput).toPath());
        BlobId blobId = BlobId.of(bucketName, blobName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build();
        Blob blob = storage.create(blobInfo, data1);
    }


    /**
     * API creata per leggere da gcs
     *
     * @param bucketName             nome del bucket root da cui si vuole leggere (specificare solo il nome del primo bucket)
     * @param relativeBucketNameBlob nome dei restanti bucket da cui si vuole leggere, compreso il nome del file che si vuole leggere
     * @return stringa letta dal file contenuto in gcs
     */
    public static String readFromGCS(String bucketName, String relativeBucketNameBlob) throws IOException {

        // Create your service object
        Storage storage = StorageOptions.getDefaultInstance().getService();
        //Blob blob = storage.get("dataproc-e233f4d8-993e-4bc1-b672-76687e9fd607-europe-west2", "primaQuery/result.txt/part-00000");
        Blob blob = storage.get(bucketName, relativeBucketNameBlob);

        //ReadChannel readChannel = blob.reader();


        /*FileOutputStream fileOuputStream = new FileOutputStream("/Users/Valerio/Downloads/primaQuery");
        fileOuputStream.getChannel().transferFrom(readChannel, 0, Long.MAX_VALUE);
        fileOuputStream.close();*/


        /*BufferedReader br = new BufferedReader(new FileReader("/Users/Valerio/Downloads/primaQuery"));
        String line = "";
        while ((line = br.readLine()) != null){
            System.out.println(line);
        }*/

        return new String(storage.readAllBytes(blob.getBlobId()), StandardCharsets.UTF_8);

    }
}
