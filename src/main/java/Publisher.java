import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.rpc.ApiException;
import com.google.cloud.ServiceOptions;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

public class Publisher {

    // use the default project id
    private static final String PROJECT_ID = ServiceOptions.getDefaultProjectId();

    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }





    /*public static void main(String[] args) throws Exception {

        Publisher.setEnv("GOOGLE_APPLICATION_CREDENTIALS","/Users/Valerio/Downloads/SABD1718-PT-a36dfe7ac719.json");

            // topic id, eg. "my-topic"
            String topicId = "TrottaPelella";
            int messageCount = 2;
            ProjectTopicName topicName = ProjectTopicName.of(PROJECT_ID, topicId);
            com.google.cloud.pubsub.v1.Publisher publisher = null;
            try {
                // Create a publisher instance with default settings bound to the topic

                publisher = com.google.cloud.pubsub.v1.Publisher.newBuilder(topicName).build();

                for (int i = 0; i < messageCount; i++) {
                    final String message = "message-" + i;

                    // convert message to bytes
                    ByteString data = ByteString.copyFromUtf8(message);
                    PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
                            .setData(data)
                            .build();



                    //schedule a message to be published, messages are automatically batched
                    ApiFuture<String> future = publisher.publish(pubsubMessage);

                    // add an asynchronous callback to handle success / failure
                    ApiFutures.addCallback(future, new ApiFutureCallback<String>() {


                        public void onFailure(Throwable throwable) {
                            if (throwable instanceof ApiException) {
                                ApiException apiException = ((ApiException) throwable);
                                // details on the API exception
                                System.out.println(apiException.getStatusCode().getCode());
                                System.out.println(apiException.isRetryable());
                            }
                            System.out.println("Error publishing message : " + message);
                        }


                        public void onSuccess(String messageId) {
                            // Once published, returns server-assigned message ids (unique within the topic)
                            System.out.println(messageId);
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (publisher != null) {
                    // When finished with the publisher, shutdown to free up resources.
                    publisher.shutdown();
                }
            }
        }*/

    /**
     * Questo metodo serve per inviare le stringhe all'interno del sistema publish subscribe di google
     *
     * @param PROJECT_ID   id del progetto su google cloud platform
     * @param topicId      id del topic sul quale inviare la stringa
     * @param stringToSend stringa da inviare
     */
    public static void sendToPubSub(String PROJECT_ID, String topicId, String stringToSend) throws Exception {


        ProjectTopicName topicName = ProjectTopicName.of(PROJECT_ID, topicId);
        com.google.cloud.pubsub.v1.Publisher publisher = null;
        try {
            // Create a publisher instance with default settings bound to the topic

            publisher = com.google.cloud.pubsub.v1.Publisher.newBuilder(topicName).build();

            // convert message to bytes
            ByteString data = ByteString.copyFromUtf8(stringToSend);
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
                    .setData(data)
                    .build();


            //schedule a message to be published, messages are automatically batched
            ApiFuture<String> future = publisher.publish(pubsubMessage);

            // add an asynchronous callback to handle success / failure
            ApiFutures.addCallback(future, new ApiFutureCallback<String>() {


                public void onFailure(Throwable throwable) {
                    if (throwable instanceof ApiException) {
                        ApiException apiException = ((ApiException) throwable);
                        // details on the API exception
                        System.out.println(apiException.getStatusCode().getCode());
                        System.out.println(apiException.isRetryable());
                    }
                    System.out.println("Error publishing message : " + stringToSend);
                }


                public void onSuccess(String messageId) {
                    // Once published, returns server-assigned message ids (unique within the topic)
                    System.out.println("messaggio inviato dal publisher con id " + messageId);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (publisher != null) {
                // When finished with the publisher, shutdown to free up resources.
                publisher.shutdown();
            }
        }


    }

}
