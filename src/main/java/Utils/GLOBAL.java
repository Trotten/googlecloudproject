package Utils;

public class GLOBAL {

    public static final String PROJECT_ID = "sabd1718-pt";

    //bigtable
    public static final String instanceId="trottapelella";

    //pubsub
    public static final String topicId1 = "query1";
    public static final String subscriptionId1 = "query1";
    public static final String topicId2 = "query2";
    public static final String subscriptionId2 = "query2";
    public static final String topicId3 = "query3";
    public static final String subscriptionId3 = "query3";
}
