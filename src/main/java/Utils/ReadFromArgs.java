package Utils;

import org.apache.commons.lang.math.NumberUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ReadFromArgs {

    public static Map<String,String> readArgs(String[] args){
        Map<String, String> map = new HashMap(args.length / 2);
        String key = null;
        String value = null;
        boolean expectValue = false;
        String[] var5 = args;
        int var6 = args.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            String arg = var5[var7];
            if (arg.startsWith("--")) {
                if (expectValue) {
                    if (value != null) {
                        throw new IllegalStateException("Unexpected state");
                    }

                    map.put(key, "__NO_VALUE_KEY");
                }

                key = arg.substring(2);
                expectValue = true;
            } else if (arg.startsWith("-")) {
                if (expectValue) {
                    if (NumberUtils.isNumber(arg)) {
                        value = arg;
                        expectValue = false;
                    } else {
                        if (value != null) {
                            throw new IllegalStateException("Unexpected state");
                        }

                        map.put(key, "__NO_VALUE_KEY");
                        key = arg.substring(1);
                        expectValue = true;
                    }
                } else {
                    key = arg.substring(1);
                    expectValue = true;
                }
            } else {
                if (!expectValue) {
                    throw new RuntimeException("Error parsing arguments '" + Arrays.toString(args) + "' on '" + arg + "'. Unexpected value. Please prefix values with -- or -.");
                }

                value = arg;
                expectValue = false;
            }

            if (value == null && key == null) {
                throw new IllegalStateException("Value and key can not be null at the same time");
            }

            if (key != null && value == null && !expectValue) {
                throw new IllegalStateException("Value expected but flag not set");
            }

            if (key != null && value != null) {
                map.put(key, value);
                key = null;
                value = null;
                expectValue = false;
            }

            if (key != null && key.length() == 0) {
                throw new IllegalArgumentException("The input " + Arrays.toString(args) + " contains an empty argument");
            }

            if (key != null && !expectValue) {
                map.put(key, "__NO_VALUE_KEY");
                key = null;
                expectValue = false;
            }
        }

        if (key != null) {
            map.put(key, "__NO_VALUE_KEY");
        }
        return map;
    }

    public static void checkPathStrings(String... strings) throws IOException {
        for (String string:strings){
            if (string==null || string.trim().isEmpty())
                throw new IOException();
        }

    }
}
