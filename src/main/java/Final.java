import Utils.GLOBAL;
import Utils.ReadFromArgs;
import com.google.cloud.bigtable.hbase.BigtableConfiguration;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class Final {


    //Questa è la parte finale del programma. Dopo aver processato il dataset con spark, si avvia questo programma.
    //Si richiede il passaggio degli argomenti contenenti il path del file delle credenziali di google, il path dei tre file restituiti in output da spark.
    //In aggiunta si possono inserire il nome delle tabelle su cui bigTable scriverà i risultati.

    //Questo programma prenderà i file scritti da spark, li leggerà e li invierà uno ad uno sul sistema PubSub di Google.
    //Successivamente verranno letti attraverso un consumer e poi inviati su BigTable.
    //È stato scelto di implementare publisher, subscriber e scrittore di bigtable tutti nello stesso programma per evitare di far partire molti main durante la sperimentazione.

    public static void main(String[] args) throws IOException {
        String pathGoogle = new String();
        String pathOutputQuery1 = new String();
        String pathOutputQuery2 = new String();
        String pathOutputQuery3 = new String();

        //leggo gli argomenti e li mappo con un chiave valore, in maniera tale da avere come chiave il --chiave e come valore il successivo valore distanziato con lo spazio
        Map<String, String> pathFiles = ReadFromArgs.readArgs(args);


        try {
            //prendo i path dei file su google cloud storage
            pathGoogle = pathFiles.get("pathGoogleCred");
            pathOutputQuery1 = pathFiles.get("pathOutQuery1");
            pathOutputQuery2 = pathFiles.get("pathOutQuery2");
            pathOutputQuery3 = pathFiles.get("pathOutQuery3");
            ReadFromArgs.checkPathStrings(pathOutputQuery1, pathOutputQuery2, pathOutputQuery3);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("usage: --pathGoogleCred ... --pathOutQuery1 ... --pathOutQuery2 ... --pathOutQuery3 ... (--tabella1 nomeTabellaQuery1 --tabella2 nomeTabellaQuery2 --tabella3 nomeTabellaQuery3)");
            System.exit(1);
        }


        //prendo i nomi delle tabelle di BigTable passati come argomenti.
        //Nel caso in cui non venissero inseriti, verrà usato di default il valore primaQuery, secondaQuery e terzaQuery
        String tabella1 = getNameTable(pathFiles, "tabella1");
        String tabella2 = getNameTable(pathFiles, "tabella2");
        String tabella3 = getNameTable(pathFiles, "tabella3");


        //setto la variabile d'ambiente con il path delle credenziali di google. Questo è necessario ai fini del funzioanemnto delle api di google
        Publisher.setEnv("GOOGLE_APPLICATION_CREDENTIALS", pathGoogle);


        try {

            //scarico da google cloud storage i file restituiti da spark e li inserisco dentro una stringa
            String stringQuery1 = GoogleCloudStorage.readFromGCS(pathOutputQuery1.substring(0, pathOutputQuery1.indexOf("/")), pathOutputQuery1.substring(pathOutputQuery1.indexOf("/") + 1));
            String stringQuery2 = GoogleCloudStorage.readFromGCS(pathOutputQuery2.substring(0, pathOutputQuery2.indexOf("/")), pathOutputQuery2.substring(pathOutputQuery2.indexOf("/") + 1));
            String stringQuery3 = GoogleCloudStorage.readFromGCS(pathOutputQuery3.substring(0, pathOutputQuery3.indexOf("/")), pathOutputQuery3.substring(pathOutputQuery3.indexOf("/") + 1));

            //stampo i risultati di spark
            System.out.println("Risultati di spark scaricandoli da gcs\n\n");
            System.out.println(stringQuery1);
            System.out.println(stringQuery2);
            System.out.println(stringQuery3);

            System.out.println("\n");

            //invio le stringhe scaricate da gcs prima su PubSub attraverso un publisher, le rileggo attraverso un subscriber e poi le invio su BigTable
            System.out.println("\nInvio su PubSub e tabella:" + tabella1 + "\n");

            sendToPubSubBigTable(stringQuery1, GLOBAL.topicId1, GLOBAL.subscriptionId1, tabella1, 1);

            System.out.println("\nInvio su PubSub e tabella:" + tabella2 + "\n");
            sendToPubSubBigTable(stringQuery2, GLOBAL.topicId2, GLOBAL.subscriptionId2, tabella2, 2);

            System.out.println("\nInvio su PubSub e tabella:" + tabella3 + "\n");
            sendToPubSubBigTable(stringQuery3, GLOBAL.topicId3, GLOBAL.subscriptionId3, tabella3, 3);


            System.out.println("\nstampo tabelle da bigTable\n");
            printAllTable(tabella1, tabella2, tabella3);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Questo metodo invia su un topic di PubSub (servizio google) la stringa passata come input.
     * Dopo aver inviato la stringa, la legge attraverso il subscriptionId e subito dopo la invia su bigTable
     * È una funzione generica, perciò in base all'interno numberOfQuery può capire a quale tabelal inviare la stringa.
     *
     * @param string         stringa da inviare sul sistema PublishSubscribe
     * @param topicId        nome del topic a cui inviare la stringa
     * @param subscriptionId nome della sottoscrizione attraverso cui riceve la stringa
     * @param nameTable      nome della tabella in cui inserire i valori
     * @param numberOfQuery  numero di query (se si sceglie 1, la si invia sulla tabella che contiene i risultati della prima query)
     */

    public static void sendToPubSubBigTable(String string, String topicId, String subscriptionId, String nameTable, int numberOfQuery) throws Exception {
        Publisher.sendToPubSub(GLOBAL.PROJECT_ID, topicId, string);
        String returnFromPubSub = SubscriberExample.readOneTimeFromPubSub(GLOBAL.PROJECT_ID, subscriptionId);
        switch (numberOfQuery) {
            case 1:
                primaQueryToBigTable(nameTable, returnFromPubSub);
                break;
            case 2:
                secondaQuery(nameTable, returnFromPubSub);
                break;
            case 3:
                terzaQuery(nameTable, returnFromPubSub);
                break;
        }


    }

    /**
     * Questo metodo prima cancella la tabella già esistente con lo stesso nome, successivamente la crea
     *
     * @param admin        admin di gcs
     * @param connection   connessione con il gcs
     * @param tableName    nome della tabella che si vuole creare
     * @param columnFamily nome delle famiglia di colonne da inserire nella tabella
     */

    public static void CreateTable(Admin admin, Connection connection, String tableName, String... columnFamily) throws IOException {
        try {
            BigTable.createTable(admin, tableName, columnFamily);
        } catch (TableExistsException e) {
            System.out.println("La tabella era già esistente, è stata resettata");
        }
    }

    /**
     * Questo metodo invia la stringa passata come input all'interno del sistema bigTable, nella tabella della primaQuery.
     * È stato scelto di non fare una funzione generica perchè c'è bisogno di mappare ogni campo della stringa nella giusta colonna
     *
     * @param tableName        nome della tabella in cui inserire i valori
     * @param returnFromPubSub stringa da inviare in BigTable
     */

    public static void primaQueryToBigTable(String tableName, String returnFromPubSub) throws IOException {

        //bigtable
        // Create the Bigtable connection, use try-with-resources to make sure it gets closed
        Connection connection = BigtableConfiguration.connect(GLOBAL.PROJECT_ID, GLOBAL.instanceId);

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

        deleteAllRowTable1(connection, tableName);
        CreateTable(admin, connection, tableName, "House_id");


        for (String string : returnFromPubSub.split("\n")) {
            BigTable.createRow(connection, tableName, "House_id", "House_id", string, string);
        }
    }

    /**
     * Questo metodo invia la stringa passata come input all'interno del sistema bigTable, nella tabella della seconda query.
     * È stato scelto di non fare una funzione generica perchè c'è bisogno di mappare ogni campo della stringa nella giusta colonna
     *
     * @param tableName        nome della tabella in cui inserire i valori
     * @param returnFromPubSub stringa da inviare in BigTable
     */

    public static void secondaQuery(String tableName, String returnFromPubSub) throws IOException {

        //bigtable
        // Create the Bigtable connection, use try-with-resources to make sure it gets closed
        Connection connection = BigtableConfiguration.connect(GLOBAL.PROJECT_ID, GLOBAL.instanceId);

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

        deleteAllRowTable2(connection, tableName);
        CreateTable(admin, connection, tableName, "Fascia", "Statistiche");

        for (String row : returnFromPubSub.split("\n")) {
            String house_id = new String(), range = new String(), media = new String(), dev_standard = new String();
            String[] singleValue = row.split(" ");
            for (String string : singleValue) {
                if (string.startsWith("house_id")) {
                    house_id = returnStringAfterPoint(string);
                } else if (string.startsWith("range")) {
                    range = returnStringAfterPoint(string);
                } else if (string.startsWith("media")) {
                    media = returnStringAfterPoint(string);
                } else if (string.startsWith("dev_standard")) {
                    dev_standard = returnStringAfterPoint(string);
                }
            }
            BigTable.createRow(connection, tableName, "Fascia", "Fascia", house_id + "/" + range, range);
            BigTable.createRow(connection, tableName, "Statistiche", "Media", house_id + "/" + range, media);
            BigTable.createRow(connection, tableName, "Statistiche", "Deviazione_Standard", house_id + "/" + range, dev_standard);

        }
    }

    /**
     * Questo metodo invia la stringa passata come input all'interno del sistema bigTable, nella tabella della terza Query.
     * È stato scelto di non fare una funzione generica perchè c'è bisogno di mappare ogni campo della stringa nella giusta colonna
     *
     * @param tableName        nome della tabella in cui inserire i valori
     * @param returnFromPubSub stringa da inviare in BigTable
     */

    public static void terzaQuery(String tableName, String returnFromPubSub) throws IOException {


        //bigtable
        // Create the Bigtable connection, use try-with-resources to make sure it gets closed
        Connection connection = BigtableConfiguration.connect(GLOBAL.PROJECT_ID, GLOBAL.instanceId);

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

        deleteAllRowTable3(connection, tableName);
        CreateTable(admin, connection, tableName, "DifferenzaFascia");


        for (String row : returnFromPubSub.split("\n")) {
            String house_id = new String(), media = new String();
            String[] singleValue = row.split(" ");
            for (String string : singleValue) {
                if (string.startsWith("key")) {
                    house_id = returnStringAfterPoint(string);
                } else if (string.startsWith("value")) {
                    media = returnStringAfterPoint(string);
                }
            }
            BigTable.createRow(connection, tableName, "DifferenzaFascia", "DifferenzaFascia", house_id, media);

        }


    }

    /**
     * Metodo che ritorna la sottostringa a partire da dopo i puntini :
     *
     * @param s stringa che si vuole parsare
     * @return la sottostringa che si vuole calcolare
     */
    public static String returnStringAfterPoint(String s) {
        return s.substring(s.indexOf(":") + 1);

    }

    public static String getNameTable(Map<String, String> pathFiles, String nameTable) {
        String tabella = pathFiles.get(nameTable);
        if (tabella == null || tabella.trim().isEmpty()) {
            if (nameTable.equals("tabella1"))
                return "primaQuery";
            else if ((nameTable.equals("tabella2")))
                return "secondaQuery";
            else return "terzaQuery";
        }
        return tabella;
    }

    /**
     * Metodo che stampa tutte le tabelle il cui nome viene passato in input
     *
     * @param tableName1 nome tabella della prima query
     * @param tableName2 nome tabella seconda query
     * @param tableName3 nome tabella terza query
     */

    public static void printAllTable(String tableName1, String tableName2, String tableName3) throws IOException {
        // Create the Bigtable connection, use try-with-resources to make sure it gets closed
        Connection connection = BigtableConfiguration.connect(GLOBAL.PROJECT_ID, GLOBAL.instanceId);

        // The admin API lets us create, manage and delete tables
        Admin admin = connection.getAdmin();

        ArrayList<String> colFamily = new ArrayList<>();
        ArrayList<String> colName = new ArrayList<>();

        colFamily.add("House_id");
        colName.add("House_id");
        System.out.println("stampo tabella " + tableName1);
        BigTable.readAllInTable(connection, tableName1, colFamily, colName);

        colFamily = new ArrayList<>();
        colName = new ArrayList<>();
        colFamily.add("Fascia");
        colFamily.add("Statistiche");
        colName.add("Fascia");
        colName.add("Media");
        colName.add("Deviazione_Standard");
        System.out.println("\nstampo tabella " + tableName2);
        BigTable.readAllInTable(connection, tableName2, colFamily, colName);

        colFamily = new ArrayList<>();
        colName = new ArrayList<>();
        colFamily.add("DifferenzaFascia");
        colName.add("DifferenzaFascia");
        System.out.println("\nstampo tabella " + tableName3);
        BigTable.readAllInTable(connection, tableName3, colFamily, colName);

    }

    /**
     * Metodo che cancella tutte le righe della prima tabella. È' stato scelto di implementare delle funzioni differenti
     * in base alla tabella per poter cancellare singolarmente tutte le colonne e le famiglie di colonne
     *
     * @param connection connessione con gcp
     * @param TABLE_NAME nome della tabella della prima query
     */

    public static void deleteAllRowTable1(Connection connection, String TABLE_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        //System.out.println("Scan for all greetings:");
        System.out.println("cancello la tabella precedente con nome: " + TABLE_NAME);
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            Delete delete = new Delete(row.getRow());
            delete.addColumns(Bytes.toBytes("House_id"), Bytes.toBytes("House_id")); // the 's' matters
            table.delete(delete);
        }

    }

    /**
     * Metodo che cancella tutte le righe della prima tabella. È' stato scelto di implementare delle funzioni differenti
     * in base alla tabella per poter cancellare singolarmente tutte le colonne e le famiglie di colonne
     *
     * @param connection connessione con gcp
     * @param TABLE_NAME nome della tabella della seconda query
     */

    public static void deleteAllRowTable2(Connection connection, String TABLE_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        //System.out.println("Scan for all greetings:");
        System.out.println("cancello la tabella precedente con nome: " + TABLE_NAME);
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            Delete delete = new Delete(row.getRow());
            delete.addColumns(Bytes.toBytes("Fascia"), Bytes.toBytes("Fascia")); // the 's' matters
            delete.addColumns(Bytes.toBytes("Statistiche"), Bytes.toBytes("Media")); // the 's' matters
            delete.addColumns(Bytes.toBytes("Statistiche"), Bytes.toBytes("Deviazione_Standard")); // the 's' matters
            table.delete(delete);
        }


    }

    /**
     * Metodo che cancella tutte le righe della prima tabella. È' stato scelto di implementare delle funzioni differenti
     * in base alla tabella per poter cancellare singolarmente tutte le colonne e le famiglie di colonne
     *
     * @param connection connessione con gcp
     * @param TABLE_NAME nome della tabella della terza query
     */
    public static void deleteAllRowTable3(Connection connection, String TABLE_NAME) throws IOException {
        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
        // Now scan across all rows.
        Scan scan = new Scan();

        //System.out.println("Scan for all greetings:");
        System.out.println("cancello la tabella precedente con nome: " + TABLE_NAME);
        ResultScanner scanner = table.getScanner(scan);
        for (Result row : scanner) {
            Delete delete = new Delete(row.getRow());
            delete.addColumns(Bytes.toBytes("DifferenzaFascia"), Bytes.toBytes("DifferenzaFascia")); // the 's' matters
            table.delete(delete);
        }


    }

}
