import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class SubscriberExample {

    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }

    // use the default project id
    private static final String PROJECT_ID = ServiceOptions.getDefaultProjectId();

    private static final BlockingQueue<PubsubMessage> messages = new LinkedBlockingDeque<PubsubMessage>();

    static class MessageReceiverExample implements MessageReceiver {

        public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {
            messages.offer(message);
            consumer.ack();
        }
    }

    /** Receive messages over a subscription. */
   /* public static void main(String... args) throws Exception {

        Publisher.setEnv("GOOGLE_APPLICATION_CREDENTIALS","/Users/Valerio/Downloads/SABD1718-PT-a36dfe7ac719.json");

        // set subscriber id, eg. my-sub
        String subscriptionId = "provaSottoscrizione";
        ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(
                PROJECT_ID, subscriptionId);
        Subscriber subscriber = null;
        try {
            // create a subscriber bound to the asynchronous message receiver
            subscriber = Subscriber.newBuilder(subscriptionName, new MessageReceiverExample()).build();
            subscriber.startAsync().awaitRunning();
            // Continue to listen to messages
            while (true) {
                PubsubMessage message = messages.take();
                System.out.println("Message Id: " + message.getMessageId());
                System.out.println("Data: " + message.getData().toStringUtf8());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }finally {
            if (subscriber != null) {
                subscriber.stopAsync();
            }
        }
    }*/

    /**
     * Questo metodo legge dal sistema publish subscribe e ritorna la stringa letta. Questo metodo legge una sola volta,
     * perciò non rimane in attesa che arrivi qualcos'altro da leggere
     *
     * @param PROJECT_ID     id del progetto su google cloud platform
     * @param subscriptionId id della sottoscrizione al topic
     */

    public static String readOneTimeFromPubSub(String PROJECT_ID, String subscriptionId) throws InterruptedException {

        // set subscriber id, eg. my-sub
        //String subscriptionId = "provaSottoscrizione";
        ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(
                PROJECT_ID, subscriptionId);
        Subscriber subscriber = null;
        try {
            // create a subscriber bound to the asynchronous message receiver
            subscriber = Subscriber.newBuilder(subscriptionName, new MessageReceiverExample()).build();
            subscriber.startAsync().awaitRunning();

            PubsubMessage message = messages.take();
            //System.out.println("Message Id: " + message.getMessageId());
            //System.out.println("Data: " + message.getData().toStringUtf8());
            System.out.println("Messaggio ricevuto dal subscriber");
            return message.getData().toStringUtf8();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (subscriber != null) {
                subscriber.stopAsync();
            }
        }


    }

    /**
     * Questo metodo serve per leggere dal sistema publish subscribe. Questo metodo consente di leggere più volte,
     * dato che rimane in attesa (con una wait asincrona) che aspetta l'arrivo di nuovi elementi pubblicati nel sistema PubSub
     *
     * @param PROJECT_ID     id del progetto su google cloud platform
     * @param subscriptionId id della sottoscrizione
     */

    public static String readFromPubSub(String PROJECT_ID, String subscriptionId) throws InterruptedException {

        // set subscriber id, eg. my-sub
        //String subscriptionId = "provaSottoscrizione";
        ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(
                PROJECT_ID, subscriptionId);
        Subscriber subscriber = null;
        try {
            // create a subscriber bound to the asynchronous message receiver
            subscriber = Subscriber.newBuilder(subscriptionName, new MessageReceiverExample()).build();
            subscriber.startAsync().awaitRunning();
            while (true) {
                PubsubMessage message = messages.take();
                System.out.println("Message Id: " + message.getMessageId());
                System.out.println("Data: " + message.getData().toStringUtf8());
                return message.getData().toStringUtf8();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (subscriber != null) {
                subscriber.stopAsync();
            }
        }


    }

}
